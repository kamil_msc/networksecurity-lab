#include "des.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "io.h"
#include "string_utils.h"
#include "binary_utils.h"

char* triple_des_encryption(char* key1,char* key2,char*message){
	char* cipher_text = des_encryption(key1,message);
	char* scrambled_text = des_decryption(key2,message);
	cipher_text = des_encryption(key1,message);
	return cipher_text;
}

char* triple_des_decryption(char* key1,char* key2,char*message){
	char* cipher_text = des_decryption(key1,message);
	char* scrambled_text = des_encryption(key2,message);
	cipher_text = des_decryption(key1,message);
	return cipher_text;
}

void triple_des_procedure(){
	printf("Apply DES first time with Key 1\n");
	printf("Apply DES again with Key2 get a scrambled text\n");
	printf("Apply DES again with key1 to get cipher text\n");
	printf("You can use three keys also \n");
}
