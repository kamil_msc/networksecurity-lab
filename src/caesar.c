#include "caesar.h"
#include "utils.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

char* caesar_substitution(char* input_text,int k,int mode){
	int MODULUS = get_modulus();
	int len = strlen(input_text);
	printf("Encrypt Plain Text = %s Key = %d Length = %d\n", input_text,k,len);
	char* output_text = (char *)malloc(sizeof(char *) * len);
	k = k % MODULUS;

	for(int i = 0;i<len;i++){
		char p = input_text[i];
		int n = get_integer(p);
		if(n != -1){
			output_text[i] = get_char(shift(n,k,MODULUS,mode));
		}else{
			output_text[i] = input_text[i];
		}
	}
	return output_text;
}


char* caesar_encryption(char* input_text,int k){
	return caesar_substitution(input_text,k,ENCRYPTION);
}

char* caesar_decryption(char* input_text,int k){
	return caesar_substitution(input_text,k,DECRYPTION);
}

void caesar_procedure(){
	printf("Ciphering-Find the ordinal of each character in the plain text, add K, take Modulus of 26 (For English Alphabets)\n");
	printf("Deciphering-Find the ordinal of each character in the cipher text, subtract K, take Modulus of 26 (For English Alphabets)\n");

}
