#include "sha_1.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "io.h"
#include "string_utils.h"
#include "binary_utils.h"
#include <stdint.h>

#define debug 0
#define rounds 80

#define SF(bits,word) \
                (((word) << (bits)) | ((word) >> (32-(bits))))

char* getRegister(char *a) {
	char *reg = (char*) malloc(sizeof(char*) * 32);
	int length = strlen(a);
	for (int i = 0; i < length; i++) {
		char *x = get_binary_from_hex(a[i]);
		for (int j = 0; j < 4; j++) {
			reg[i * 4 + j] = x[j];
		}
	}
	return reg;
}

uint32_t add_32(uint32_t a, uint32_t b, uint32_t c, uint32_t d, uint32_t e) {
	uint32_t sum = a + b + c + d + e;
	//printf("\n %x + %x + %x +  %x + %x = %x\n",a,b,c,d,e,sum);
	return sum;
}

char* add(char *a, char *f, char *e, char *k, char *we) {
	uint32_t w[5];
	w[0] = get_decimal(a);
	w[1] = get_decimal(f);
	w[2] = get_decimal(e);
	w[3] = get_decimal(k);
	w[4] = get_decimal(we);

	uint32_t sum = add_32(w[0], w[1], w[2], w[3], w[4]);
	//printf("\nSUM %x %s \n",sum,get_binary(sum,32));
	return get_binary(sum, 32);
}

char* add2(char *a, char *b) {
	return get_binary(get_decimal(a) + get_decimal(b), 32);
}

char** parse(char *text) {
	char *b_512 = (char*) malloc(sizeof(char*) * 512);
	for (int i = 0; i < 512; i++)
		b_512[i] = '0';
	int length = strlen(text);
	for (int i = 0; i < length; i++) {
		int d = text[i];
		char *b = get_binary(d, 8);
		for (int j = 0; j < 8; j++) {
			b_512[i * 8 + j] = b[j];
		}
	}
	b_512[length * 8] = '1';
	char *l = get_binary(length * 8, 64);
	//block_print(l, 8);
	for (int i = 0; i < 64; i++) {
		b_512[448 + i] = l[i];
	}
	//block_print(b_512, 8);
	//print_line();
	char **W = (char**) malloc(sizeof(char**) * 80);
	for (int i = 0; i < 16; i++) {
		W[i] = (char*) malloc(sizeof(char*) * 32);
		for (int j = 0; j < 32; j++) {
			W[i][j] = b_512[i * 32 + j];
		}
	}
	for (int i = 16; i < 80; i++) {
		W[i] = (char*) malloc(sizeof(char*) * 32);
		W[i] = left_rotate(
				xor_word(xor_word(xor_word(W[i - 3], W[i - 8]), W[i - 14]),
						W[i - 16]), 1);
	}
	if(debug)
		printf("Input Message is consumed and placed into the 80*32 word blocks as per spec\n");
	return W;

}

char** get_intermediate_hash() {
	char **H = (char**) malloc(sizeof(char**) * 5);
	for (int i = 0; i < 5; i++) {
		H[i] = (char*) malloc(sizeof(char*) * 32);
	}
	strcpy(H[0], getRegister("67452301"));
	strcpy(H[1], getRegister("EFCDAB89"));
	strcpy(H[2], getRegister("98BADCFE"));
	strcpy(H[3], getRegister("10325476"));
	strcpy(H[4], getRegister("C3D2E1F0"));
	if(debug)
		printf("Intermediate Hash Prepared...\n");
	return H;
}

char** get_k() {
	char **K = (char**) malloc(sizeof(char**) * 4);
	for (int i = 0; i < 4; i++) {
		K[i] = (char*) malloc(sizeof(char*) * 32);
	}
	strcpy(K[0], getRegister("5A827999"));
	strcpy(K[1], getRegister("6ED9EBA1"));
	strcpy(K[2], getRegister("8F1BBCDC"));
	strcpy(K[3], getRegister("CA62C1D6"));
	if(debug)
		printf("K Generated\n");
	return K;
}

int Xlen(char *S){
	int len = 0;
	int length = strlen(S);
	for(int i = 0;i<length;i++){
		if(S[i] == ' '){
			break;
		}
		len = len + 1;
	}
	return len;
}

char* pack_as_hex(char** H){
	int total_length = 0;
	for(int i = 0;i<5;i++){
		total_length += Xlen(get_hex(H[i]));
	}
	char *sha1 = (char*) malloc(sizeof(char*) * total_length);
	int r = 0;
	for(int i = 0;i<5;i++){
		char* X = get_hex(H[i]);
		int len = strlen(X);
		for(int j = 0;j<len;j++){
			if(X[j] == ' ')break;
			sha1[r++] = X[j];
		}
	}
	return sha1;
}

char* digest(char *text) {
	if(debug)
		printf("String Technique...\n");
	char **W = parse(text);
	char **H = get_intermediate_hash();
	char **K = get_k();

	char *a = (char*) malloc(sizeof(char*) * 32);
	char *b = (char*) malloc(sizeof(char*) * 32);
	char *c = (char*) malloc(sizeof(char*) * 32);
	char *d = (char*) malloc(sizeof(char*) * 32);
	char *e = (char*) malloc(sizeof(char*) * 32);
	strcpy(a, H[0]);strcpy(b, H[1]);strcpy(c, H[2]);strcpy(d, H[3]);strcpy(e, H[4]);

	char *f;
	char *k;

	for (int i = 0; i < rounds; i++) {
		if (i >= 0 && i <= 19) {
			char *b_not = not_word(b);
			char *f_and = and_word(b, c);
			char *s_and = and_word(b_not, d);
			f = or_word(f_and, s_and);
			k = K[0];
			//printf("A %s B %s C %s D %s E %s W %s OR %s K %s", get_hex(a), get_hex(b),
			//		get_hex(c), get_hex(d), get_hex(e), get_hex(W[i]), get_hex(f),get_hex(k));
		}

		if (i >= 20 && i <= 39) {
			f = xor_word(b, xor_word(c, d));
			k = K[1];
		}

		if (i >= 40 && i <= 59) {
			f = or_word(or_word(and_word(b, c), and_word(b, d)),
					and_word(c, d));
			k = K[2];
		}

		if (i >= 60 && i <= 79) {
			f = xor_word(b, xor_word(c, d));
			k = K[3];
		}
		char *temp = add(left_rotate(a, 5), f, e, k, W[i]);
		strcpy(e, d);
		strcpy(d, c);
		c = left_rotate(b, 30);
		strcpy(b, a);
		strcpy(a, temp);
	}
	if(debug)
		printf("Rounds completed\n");
	H[0] = add2(H[0], a);
	H[1] = add2(H[1], b);
	H[2] = add2(H[2], c);
	H[3] = add2(H[3], d);
	H[4] = add2(H[4], e);
	return pack_as_hex(H);
}

char* digest_int(char *text) {
	if(debug)
		printf("Integer Technique...\n");
	char **W = parse(text);
	uint32_t w;
	uint32_t F, K;

	uint32_t H0 = 0x67452301;
	uint32_t H1 = 0xEFCDAB89;
	uint32_t H2 = 0x98BADCFE;
	uint32_t H3 = 0x10325476;
	uint32_t H4 = 0xC3D2E1F0;

	uint32_t A = H0;
	uint32_t B = H1;
	uint32_t C = H2;
	uint32_t D = H3;
	uint32_t E = H4;

	uint32_t K0 = 0x5A827999;
	uint32_t K1 = 0x6ED9EBA1;
	uint32_t K2 = 0x8F1BBCDC;
	uint32_t K3 = 0xCA62C1D6;
	uint32_t TEMP;

	for (int i = 0; i < rounds; i++) {
		w = get_decimal(W[i]);
		//printf("A %x B %x C %x D %x E %x w %x OR %x K %x", A, B, C, D, E, w[i], ((B & C) | ((~B) & D)), K0);
		if (i >= 0 && i <= 19) {
			F = ((B & C) | ((~B) & D));
			K = K0;
		}
		if (i >= 20 && i <= 39) {
			F = B ^ C ^ D;
			K = K1;
		}
		if (i >= 40 && i <= 59) {
			F = (B & C) | (B & D) | (C & D);
			K = K2;
		}
		if (i >= 60 && i <= 79) {
			F = B ^ C ^ D;
			K = K3;
		}
		TEMP = add_32(SF(5, A), F, E, K, w);
		E = D;
		D = C;
		C = SF(30, B);
		B = A;
		A = TEMP;
		//printf(" TEMP %x \n", TEMP);
	}
	if(debug)
		printf("Rounds completed\n");
	H0 = H0 + A;
	H1 = H1 + B;
	H2 = H2 + C;
	H3 = H3 + D;
	H4 = H4 + E;
	char **H = (char**) malloc(sizeof(char**) * 5);
	for (int i = 0; i < 5; i++) {
		H[i] = (char*) malloc(sizeof(char*) * 32);
	}
	H[0] = get_binary(H0,32);H[1] = get_binary(H1,32);
	H[2] = get_binary(H2,32);H[3] = get_binary(H3,32);
	H[4] = get_binary(H4,32);
	return pack_as_hex(H);
}

void sha_1_procedure() {
	printf("Initialize Random 5 HEX values.\n");
	printf("Message is padded with 1 followed by 0's till 448 bits.\n");
	printf("length of message represented by 64 bits is appended at the last.\n");
	printf("512 bits is expanded to 2560 bits (80*32) - 80 words of 32 bit length\n");
	printf("80 Rounds should run with the specific operations.\n");
	printf("Random Hex is added with A,B,C,D,E and SHA-1 digest is calculated as 160 bits.\n");
}
