#include "diffie_hellman.h"
#include "utils.h"
#include "math_utils.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>


int get_public_exchange_number(int P,int Q,int private_key){
	return 	power_mod(Q,private_key, P);
}

int get_secret_key(int exchanged_no,int private_key,int P){
	return power_mod(exchanged_no,private_key,P);
}

void diffie_hellman_procedure(){
	printf("Used to Send the Key secretly over the network\n");
	printf("https://en.wikipedia.org/wiki/Diffie_Hellman_key_exchange\n");
	printf("Choose P and Q such that P is Prime and gcd(P,Q) = 1. P,Q can be visible to the world\n");
	printf("Let A and B (Communicating Parties) choose their own private key \n");
	printf("They can calculate the public exchange number by doing power(Q,private_key) mod P.\n");
	printf("Public Exchange Number can be visible to outer word and let A and B exchange their numbers visible to others\n");
	printf("Communicating Parties can derive secret key by calculating power(exchange_no,private_key) mod P\n");
}
