/*
 * binary_utils.c
 *
 *  Created on: 20-Jul-2020
 *      Author: kkhan
 */

#include "binary_utils.h"
#include "math_utils.h"
#include "string_utils.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

uint32_t get_decimal(char* str){
	uint32_t decimal = 0;
	int len = strlen(str);
	int i = 0;
	while(i< len){
		char c = str[len-i-1];
		if(c == '1'){
			decimal = decimal + power(2,i);
		}
		i = i + 1;
	}
	//printf("Get Decimal %s %d \n",str,decimal);
	return decimal;
}

char* get_binary(uint32_t no,int length){
	char *s = (char *)malloc(sizeof(char *)*length);
	for(int j = 0;j<length;j++){
		s[j] = '0';
	}

	while(length > 0){
		int mod = no % 2;
		if(mod){
			s[length-1] = '1';
		}else{
			s[length-1] = '0';
		}
		no = no/2;
		length = length-1;
	}
	return s;
}

char hex(char *s){
	if(strcmp(s,"0000")==0) return '0';
	if(strcmp(s,"0001")==0) return '1';
	if(strcmp(s,"0010")==0) return '2';
	if(strcmp(s,"0011")==0) return '3';
	if(strcmp(s,"0100")==0) return '4';
	if(strcmp(s,"0101")==0) return '5';
	if(strcmp(s,"0110")==0) return '6';
	if(strcmp(s,"0111")==0) return '7';
	if(strcmp(s,"1000")==0) return '8';
	if(strcmp(s,"1001")==0) return '9';
	if(strcmp(s,"1010")==0) return 'A';
	if(strcmp(s,"1011")==0) return 'B';
	if(strcmp(s,"1100")==0) return 'C';
	if(strcmp(s,"1101")==0) return 'D';
	if(strcmp(s,"1110")==0) return 'E';
	if(strcmp(s,"1111")==0) return 'F';
	return ' ';
}

char* get_binary_from_hex(char hex){
	switch(hex){
		case '0': return "0000";
		case '1': return "0001";
		case '2': return "0010";
		case '3': return "0011";
		case '4': return "0100";
		case '5': return "0101";
		case '6': return "0110";
		case '7': return "0111";
		case '8': return "1000";
		case '9': return "1001";
		case 'A': return "1010";
		case 'B': return "1011";
		case 'C': return "1100";
		case 'D': return "1101";
		case 'E': return "1110";
		case 'F': return "1111";
		case 'a': return "1010";
		case 'b': return "1011";
		case 'c': return "1100";
		case 'd': return "1101";
		case 'e': return "1110";
		case 'f': return "1111";
	}
	return "";
}

int get_decimal_from_hex(char hex){
	switch(hex){
		case '0': return 0;
		case '1': return 1;
		case '2': return 2;
		case '3': return 3;
		case '4': return 4;
		case '5': return 5;
		case '6': return 6;
		case '7': return 7;
		case '8': return 8;
		case '9': return 9;
		case 'A': return 10;
		case 'B': return 11;
		case 'C': return 12;
		case 'D': return 13;
		case 'E': return 14;
		case 'F': return 15;
		case 'a': return 10;
		case 'b': return 11;
		case 'c': return 12;
		case 'd': return 13;
		case 'e': return 14;
		case 'f': return 15;
	}
	return 0;
}

void print_hex(char *E) {
	printf("HEX: ");
	char *CX = (char*) malloc(sizeof(char*) * 16);
	int len = strlen(E)/4;
	for (int i = 0; i < len; i++) {
		CX[i] = hex(substr(E, i * 4, (i + 1) * 4));
	}
	printf("%s\n", CX);
}

char* get_hex(char *B){
	int len = strlen(B);
	len = len/2;
	char *CX = (char*) malloc(sizeof(char*) * len);
	for (int i = 0; i < 16; i++) {
		CX[i] = hex(substr(B, i * 4, (i + 1) * 4));
	}
	return CX;
}

char* get_character_from_binary(char *B){
	int len = strlen(B)/8;
	char* M = (char *)malloc(sizeof(char *)* len);
	for(int i = 0;i<len;i++){
		//printf("%d\n",);
		char c = get_decimal(substr(B,i*8,(i+1)*8));
		M[i] = c;
	}
	return M;
}

char* get_64_bit_payload(char *message) {
	int len = strlen(message);
	int L = len;
	if (len % 8 != 0) {
		L = len + 8 - (len % 8);
	}
	char *M = (char*) malloc(sizeof(char*) * (L * 8));
	for (int i = 0; i < L; i++) {
		for (int j = 0; j < 8; j++) {
			int pos = i * 8 + j;
			M[pos] = '0';
		}
	}
	for (int i = 0; i < len; i++) {
		int d = message[i];
		char *X = get_binary(d, 8);
		int pos = (i * 8);
		for (int j = 0; j < 8; j++) {
			M[pos + j] = X[j];
		}
	}
	return M;
}

char xor(char a, char b) {
	if (a == b)
		return '0';
	return '1';
}

char and(char a, char b) {
	if (a == '1' && b == '1')
		return '1';
	return '0';
}

char or(char a, char b) {
	if (a == '1' || b == '1')
		return '1';
	return '0';
}

char not(char a) {
	if (a == '1')
		return '0';
	return '1';
}



char* xor_word(char *a, char *b) {
	int len = strlen(a);
	char* c = (char *)malloc(sizeof(char *)* len);
	for(int i = 0;i<len;i++){
		c[i] = xor(a[i],b[i]);
	}
	return c;
}

char* and_word(char *a, char *b) {
	int len = strlen(a);
	char* c = (char *)malloc(sizeof(char *)* len);
	for(int i = 0;i<len;i++){
		c[i] = and(a[i],b[i]);
	}
	return c;
}

char* or_word(char *a, char *b) {
	int len = strlen(a);
	char* c = (char *)malloc(sizeof(char *)* len);
	for(int i = 0;i<len;i++){
		c[i] = or(a[i],b[i]);
	}
	return c;
}

char* not_word(char *a) {
	int len = strlen(a);
	char* c = (char *)malloc(sizeof(char *)* len);
	for(int i = 0;i<len;i++){
		c[i] = not(a[i]);
	}
	return c;
}

char* left_rotate(char *a,int len){
	int length = strlen(a);
	char* c = (char *)malloc(sizeof(char *)* length);
	char* begin = substr(a,0,len);
	for(int i = len;i<length;i++){
		c[i-len] = a[i];
	}
	for(int i = 0;i<len;i++){
		c[length-len+i] = begin[i];
	}
	return c;
}

char* get_binary_from_hex_string(char* hex_string){
	int len = strlen(hex_string) * 4;
	char* c = (char *)malloc(sizeof(char *)*len);
	for(int i = 0;i<len;i++){
		char* X = get_binary_from_hex(hex_string[i]);
		for(int j = 0;j<4;j++){
			c[i*4 + j] = X[j];
		}
	}
	return c;
}

int get_hex_from_binary(char *bin){
	char *a = bin;
	int num = 0;
	do {
	    int b = *a=='1'?1:0;
	    num = (num<<1)|b;
	    a++;
	} while (*a);
	return num;
}




