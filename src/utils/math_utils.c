/*
 * math_utils.c
 *
 *  Created on: 3-Aug-2020
 *      Author: kkhan
 */

#include "math_utils.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

int is_prime(int no){
	for(int i = 2;i<= no/2;i++){
		if(no % i == 0) return 0;
	}
	return 1;
}

int gcd_internal(int a,int b){
	int r = a % b;
	//printf("%d %d %d\n",a,b,r);
	if (r == 0){
		return b;
	}
	return gcd_internal(b,r);
}

int gcd(int a,int b){
	if (b > a){
		int c = b;
		b = a;
		a = c;
	}
	return gcd_internal(a,b);
}

int multiplicative_inverse(int no, int mod){
	for(int i = 1;i<=mod;i++){
		if((i * no) % mod == 1){
			return i;
		}
	}
	return 0;
}


int greatest_power_of_2(int no){
	if(no == 1){
		return 1;
	}
	int p = 1;
	while(no >= p){
		p = p * 2;
	}
	return p/2;
}

int highest_bit_location(int no){
	int i = 0;
	while(no){
		no = no >> 1;
		i = i + 1;
	}
	return i;
}

int mod(int a,int b){
	return a % b;
}


int inverse(int D,int modulus){
	for(int i = 1;i<100;i++){
		int mod = (D * i) % modulus;
		if(mod == 1){
			return i;
		}
	}
	return 1;
}

uint32_t power(int a,int b){
	uint32_t p = 1;
	for(int i = 1;i<=b;i++){
		p = p * a;
	}
	return p;
}


int power_mod(int a,int b,int MOD){
	//printf("%d , %d , %d\n ",a,b,MOD);
	int p = 1;
	int mod_value = 1;
	for(int i = 1;i<=b;i++){
		p = p * a;
		if(p >= MOD){
			//printf(" crossed at %d\n",i);
			mod_value = mod(mod_value * mod(p,MOD),MOD);
			p = 1;
		}
	}

	return mod(mod_value*p,MOD);
}
