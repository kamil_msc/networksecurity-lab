/*
 * utils.h
 *
 *  Created on: 20-Jul-2020
 *      Author: kkhan
 */

#include "utils.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

char* append_char_if_odd(char* stg,char c){
	int len = strlen(stg);
	if(len % 2 == 1){
		char* new_stg = (char *)malloc(sizeof(char) * len);
		strcpy(new_stg,stg);
		new_stg[len] = 'x';
		return new_stg;
	}
	char* new_stg = (char *)malloc(sizeof(char) * len);
	strcpy(new_stg,stg);
	return new_stg;
}

int is_char_not_present(char *stg,char c){
	for(int i = 0;i<strlen(stg);i++){
		if(stg[i] == c){
			return 0;
		}
	}
	return 1;
}


char* replace(char* word,char s,char d){
	for(int i = 0;i<strlen(word);i++){
		if(word[i] == s){
			word[i] = d;
		}
	}
	return word;
}

int reduce(int c,int v){
	while(c < 0){
		c = c + v;
	}
	return c;
}


char* substring(char *string,int index,int length){
	char* s = (char *)malloc(sizeof(char) * length);
	int len = strlen(string);
	for(int j = 0;j<length;j++){
		if(index + j < len){
			s[j] = string[index + j];
		}else{
			s[j] = 'a';
		}
	}
	return s;
}

char map[] = { 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q',
		'r','s','t','u','v','w','x','y','z'
};
int map_length = 26;

int get_integer(char c){
	for(int i = 0;i<map_length;i++){
		if(c == map[i]) return i;
	}
	return -1;
}


char get_char(int no){
	return map[no];
}

int get_modulus(){
	return map_length;
}


int shift(int n,int k,int MODULUS,int mode){
	int x = 0;
	if(mode == ENCRYPTION){
		return (n + k) % MODULUS;
	}
	return reduce(n-k,MODULUS) % MODULUS;
}




