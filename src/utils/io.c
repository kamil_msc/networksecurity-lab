#include "io.h"
#include "math_utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* get_input(char *message){
	char* input = (char *)malloc(sizeof(100));
	printf("%s...",message);
	scanf("%s",input);
	return input;
}

int get_int_input(char *message){
	int input;
	printf("%s...",message);
	scanf("%d",&input);
	return input;
}


int get_prime(char* message){
	int n = get_int_input(message);
	while(!is_prime(n)){
		printf("%d is not Prime\n",n);
		n = get_int_input(message);
	}
	return n;
}

void print_line(){
	printf("\n");
	for(int i = 0;i<200;i++) printf("-");
	printf("\n");

}

int get_random(int l,int u){
	printf("Enter between %d and %d randomly ",l,u);
	int random = get_int_input("");
	return random;
}

void print_binary_string(char *s){
	printf("\n");
	int len = strlen(s);
	for(int i =0;i<len;i++){
		if(i % 8 == 0){
			printf(" ");
		}
		printf("%c",s[i]);
	}
	printf("\n");

}
