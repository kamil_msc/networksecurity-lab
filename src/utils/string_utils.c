/*
 * string_utils.c
 *
 *  Created on: 20-Jul-2020
 *      Author: kkhan
 */

#include "string_utils.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

void concat(char *dst, char *src_1, char* src_2){
	int l1 = strlen(src_1);
	int l2 = strlen(src_2);
	for(int i = 0;i<l1;i++){
		dst[i] = src_1[i];
	}
	for(int i = 0;i<l2;i++){
		dst[l1+i] = src_2[i];
	}
}

void copy(char *src, int s, int e, char*dst){
	int j = 0;
	for(int i = s;i<e;i++){
		dst[j++] = src[i];
	}
}


void block_print(char *s,int block_length){
	block_print_ext(s,block_length,"");
}

void block_print_ext(char *s,int block_length,char* verbose){
	int len = strlen(s);

	for(int i = 0;i<len;i++){
		if(i % block_length == 0 && i != 0){
			printf("   ");
		}
		printf("%c",s[i]);
	}
	printf(" %d %s\n",len,verbose);
}


char* substr(char *er, int s, int e) {
	int length = e - s;
	char *r = (char*) malloc(sizeof(char) * length);
	for (int i = 0; i < length; i++) {
		r[i] = er[s + i];
	}
	return r;
}



