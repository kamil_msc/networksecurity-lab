#include <stdio.h>
#include <stdlib.h>

int is_not_present_in_matrix(char** matrix,char c,int n){
	for(int i = 0;i<n;i++){
		for(int j = 0;j<n;j++){
			if(matrix[i][j] == c){
				return 0;
			}
		}
	}
	return 1;
}

void print_char_matrix(char** matrix,int n){
	for(int i = 0;i<n;i++){
		for(int j = 0;j<n;j++){
			printf("%c\t",matrix[i][j]);
		}
		printf("\n");
	}
}

void init_matrix(char** matrix,int n){
	for(int i = 0;i<n;i++){
		for(int j = 0;j<n;j++){
			matrix[i][j] = ' ';
		}
	}
}


void get_pos(char** matrix,int *r,int *c,char x,int n){
	for(int i = 0;i<n;i++){
		for(int j = 0;j<n;j++){
			if(matrix[i][j] == x){
				*r = i;
				*c = j;
				return;
			}
		}
	}
}

char get_right(char** matrix,int r,int c,int n){
	int c_dash = c + 1;
	if(c_dash == n) c_dash = 0;
	return matrix[r][c_dash];
}


char get_down(char** matrix,int r,int c,int n){
	int r_dash = r + 1;
	if(r_dash == n) r_dash = 0;
	return matrix[r_dash][c];
}

char get_left(char** matrix,int r,int c,int n){
	int c_dash = c - 1;
	if(c_dash == -1) c_dash = 4;
	return matrix[r][c_dash];
}


char get_up(char** matrix,int r,int c,int n){
	int r_dash = r - 1;
	if(r_dash == -1) r_dash = 4;
	return matrix[r_dash][c];
}



int** reduce_matrix(int** matrix,int i_index,int j_index,int n){
	int** rm = (int **)malloc(sizeof(int *) * n-1);
	for(int i = 0;i<n;i++){
		rm[i] = (int *)malloc(sizeof(int) * n-1);
	}
	int i1 = 0;
	for(int i = 0;i<n;i++){
		if(i == i_index) continue;
		int j1 = 0;
		for(int j = 0;j<n;j++){
			if(j == j_index) continue;
			rm[i1][j1] = matrix[i][j];
			j1 = j1 + 1;
		}
		i1 = i1 + 1;
	}
	return rm;
}

int** strip_0(int** matrix,int j_index,int n){
	return reduce_matrix(matrix,0,j_index,n);
}

void print_matrix_2d(int **matrix,int n){
	for(int i = 0;i<n;i++){
		for(int j = 0;j<n;j++){
			printf("%d ",matrix[i][j]);
		}
		printf("\n");
	}
}

int determinant(int** matrix,int n){
	if(n == 1){
		return matrix[0][0];
	}
	if(n == 2){
		return (matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0]);
	}
	int sum = 0;
	int X = 1;
	for(int i = 0;i<n;i++){
		int M = matrix[0][i];
		int D = determinant(reduce_matrix(matrix,0,i,n),n-1);
		int I = X * matrix[0][i] * D;
		sum = sum + I;
		X = X * -1;
	}
	return sum;
}


int sign_of_position(int i,int j){
	if((i+j) % 2 == 0){
		return 1;
	}
	return -1;
}

