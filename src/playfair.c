#include "playfair.h"
#include "utils.h"
#include "matrix.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define N 5

void reset(int *i,int *j,int n){
	if(*j == N){
		*j = 0;
		*i = *i + 1;
	}
}

void assign_key_in_matrix(char** matrix,char *key,int *i,int *j){
	for(int pos = 0;pos<strlen(key);pos++){
		reset(i,j,N);
		if(is_not_present_in_matrix(matrix,key[pos],N)){
			matrix[*i][*j] = key[pos];
			*j = *j + 1;
		}
	}
}

void assign_remaining_alphabets(char** matrix,int *i,int *j){
	for(char k = 'a'; k <= 'z';k++){
		if(k == 'j') continue;
		reset(i,j,N);
		if(is_not_present_in_matrix(matrix,k,N)){
			matrix[*i][*j] = k;
			*j = *j + 1;
		}
	}
}

char* playfair_encryption(char* plain_text,char* key){
	printf("Encrypt Plain Text = %s Key = %s\n", plain_text,key);
	char** matrix = (char **)malloc(sizeof(char **) * N);
	for(int i = 0;i<N;i++){
		matrix[i] = (char *)malloc(sizeof(char *) * N);
	}
	init_matrix(matrix,N);
	int i = 0,j=0;
	key = replace(key,'j','i');
	assign_key_in_matrix(matrix,key,&i,&j);
	assign_remaining_alphabets(matrix,&i,&j);

	plain_text = append_char_if_odd(plain_text,'x');
	plain_text = replace(plain_text,'j','i');
	i = 0;
	int len = strlen(plain_text);
	printf("Length %d\n",len);
	print_char_matrix(matrix,N);
	while(i < len){
		int r1, c1, r2, c2;
		get_pos(matrix,&r1,&c1,plain_text[i],N);
		printf("P2 = %c R1 = %d  C1 = %d\n",plain_text[i],r1,c1);
		get_pos(matrix,&r2,&c2,plain_text[i+1],N);
		printf("P2 = %c R2 = %d  C2 = %d\n",plain_text[i+1],r2,c2);

		if(r1 == r2){
			plain_text[i] = get_right(matrix,r1,c1,N);
			plain_text[i+1] = get_right(matrix,r1,c2,N);
		}else
		if(c1 == c2){
			plain_text[i] = get_down(matrix,r1,c1,N);
			plain_text[i+1] = get_down(matrix,r2,c1,N);
		}else{
			plain_text[i] = matrix[r1][c2];
			plain_text[i+1] = matrix[r2][c1];
		}

		i = i + 2;
	}
	return plain_text;
}


char* playfair_decryption(char* cipher_text,char* key){
	printf("Decrypt Cipher = %s Key = %s\n", cipher_text,key);
	char** matrix = (char **)malloc(sizeof(char **) * N);
	for(int i = 0;i<N;i++){
		matrix[i] = (char *)malloc(sizeof(char *) * N);
	}
	init_matrix(matrix,N);
	int i = 0,j=0;
	assign_key_in_matrix(matrix,key,&i,&j);
	assign_remaining_alphabets(matrix,&i,&j);
	print_char_matrix(matrix,N);
	i = 0;

	while(i < strlen(cipher_text)){
		int r1, c1, r2, c2;
		get_pos(matrix,&r1,&c1,cipher_text[i],N);
		get_pos(matrix,&r2,&c2,cipher_text[i+1],N);
		if(r1 == r2){
			cipher_text[i] = get_left(matrix,r1,c1,N);
			cipher_text[i+1] = get_left(matrix,r1,c2,N);
		}else
		if(c1 == c2){
			cipher_text[i] = get_up(matrix,r1,c1,N);
			cipher_text[i+1] = get_up(matrix,r2,c1,N);
		}else{
			cipher_text[i] = matrix[r1][c2];
			cipher_text[i+1] = matrix[r2][c1];
		}

		i = i + 2;
	}
	return cipher_text;
}

void playfair_procedure(){

}

