#include "dsa.h"
#include "utils.h"
#include "math_utils.h"
#include "io.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int hash(char *M){
	return 41;
}

void generate_dsa_key(int p,int q,int g,int *a,int *b){
	printf("\nKey Generation Phase Started\n");
	*a = get_random(1,q-1);
	*b = power_mod(g,*a,p);
	printf("Private Key %d Public Key %d",*a,*b);
	print_line();
}

void generate_signature(int p,int q,int g,int signer_private_key,char *M,int *r,int *s){
	printf("Signature Generation Phase Started\n");
	int k = get_random(1,q-1);
	int X = power_mod(g,k,p);
	*r = mod(X,q);
	int k_inverse = multiplicative_inverse(k,q);
	int h = hash(M);
	*s = mod(k_inverse*(h + signer_private_key**r),q);
	printf("(r,s) = (%d,%d)",*r,*s);
	print_line();
}

int verify_signature(int p,int q,int g,int signer_public_key,char *SM,int r,int s){
	printf("Signature Verification Phase Started. Need to check against %d \n",r);
	int w = multiplicative_inverse(s,q);
	int h = hash(SM);
	int u1 = mod(h*w,q);
	int u2 = mod(r*w,q);
	int X = mod(power_mod(g,u1,p) * power_mod(signer_public_key,u2,p),p);
	int v = mod(X,q);
	//printf("%d %d %d \%d %d \n",w,u1,u2,X,v);
	printf("calculated v = %d \n",v);
	if(v == r){
		printf("ACCEPT the message");
		return 1;
	}
	printf("REJECT the message");
	print_line();
	return -1;
}

void dsa_procedure(){
	printf("The DSA algorithm involves four operations: key generation (which creates the key pair), key distribution, signing and signature verification.\n");

}


