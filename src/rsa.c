// RSA Stands for Rivest–Shamir–Adleman

#include "rsa.h"
#include "io.h"
#include "utils.h"
#include "math_utils.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>


int binary_split(int no,int *mod_array,int n){
	int p = 1;
	printf("\n");
	while(no >= 1){
		int x = greatest_power_of_2(no);
		int loc = highest_bit_location(x);
		printf("(%d) %d * %d mod %d = ",x,p,mod_array[loc-1],n);
		p = p * mod_array[loc-1];
		p = p % n;
		printf("%d\n",p);
		no = no - x;
	}

	return p;
}


int rsa_process(int message,int e, int n){
	print_line();
	printf("Find %d ^ %d  mod %d using exponentiation by squaring\n",message,e,n);
	int g2 = greatest_power_of_2(e);
	int loc = highest_bit_location(g2);
	int* mod_array = (int *)malloc(sizeof(int)*loc);
	mod_array[0] = message;
	printf("%d mod %d = %d\n",message,n,mod_array[0]);
	for(int i = 1;i<loc;i++){
		int prev_mod = mod_array[i-1];
		mod_array[i] = (prev_mod * prev_mod) % n;
		printf("%d * %d mod %d = %d\n",prev_mod,prev_mod,n,mod_array[i]);
	}
	for(int i = 0;i<loc;i++){
		printf("%d(%d) ",mod_array[i],(int)pow(2,i));
	}
	printf("\n\n");
	int p = binary_split(e,mod_array,n);
	print_line();
	return p % n;
}

int rsa_encryption(int message,int e,int n){
	return rsa_process(message,e,n);
}


int rsa_decryption(int message,int d,int n){
	return rsa_process(message,d,n);
}


int get_e(int tc){
	int e = 0;
	int gcd_e_tc = 0;
	do{
		printf("Enter e such that 1 < e < %d and gcd(e,%d) = 1 e = ",tc,tc);
		scanf("%d",&e);
		if(e <= 1) {
			printf("e is less than or equal to 1\n");
			continue;
		}
		if(e > tc){
			printf("e is greater than totient quotient\n");
		}
		gcd_e_tc = gcd(e,tc);
		if(gcd_e_tc != 1) printf("e and totient quotient should be relative prime. "
				"The GCD(%d,%d) is %d\n",e,tc,gcd_e_tc);
		if(e > 1 && e < tc && gcd_e_tc == 1)break;
	}while(1);
	return e;
}

void generate_key(int p,int q,int *e,int *d,int *tc){
	do{
		*tc = (p-1)*(q-1);
		printf("Totient Coefficient %d\n",*tc);
		*e = get_e(*tc);
		*d = multiplicative_inverse(*e,*tc);
		if(*d == 0){
			printf("Could not find multiplicative inverse of %d under %d\n",*e,*tc);
		}
	}while(*d == 0);
	printf("Multiplicative Inverse of %d = %d\n",*e,*d);
}


void rsa_procedure(){
	printf("Rivest–Shamir–Adleman - Encrypt and Decipher - Asymmetric Cryptography.\n");
	printf("https://simple.wikipedia.org/wiki/RSA_algorithm\n");
	printf("Public Key can be known to every body\n");
	printf("Message intended to me be encrypted using my public key by others and I can only read it\n");
	printf("Get P and Q both being Prime. Find N = p * q. Find Totient Quotiet = (p-1)*(q-1)\n");
	printf("Get E such that gcd(e,totient_quotient) = 1\n. E is public key\n");
	printf("Find D such that de congruent modulo to totient quotient (Multiplicative Inverse)\n");
	printf("D is private key\n");
	printf("Cipher power(p,e) mod n");
	printf("DeCipher power(c,d) mod n");



}
