#include "des.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "io.h"
#include "string_utils.h"
#include "binary_utils.h"

#define debug 0
int pc1[56] = { 57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59,
		51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36, 63, 55, 47, 39, 31, 23, 15,
		7, 62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20,
		12, 4 };
int shift_array[16] = { 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 };
int pc2[48] = { 14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4, 26,
		8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40, 51, 45, 33, 48,
		44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32 };

int ip1[64] = { 58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4,
		62, 54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16, 8, 57, 49,
		41, 33, 25, 17, 9, 1, 59, 51, 43, 35, 27, 19, 11, 3, 61, 53, 45, 37, 29,
		21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7, };
int ebit[48] = { 32, 1, 2, 3, 4, 5, 4, 5, 6, 7, 8, 9, 8, 9, 10, 11, 12, 13, 12,
		13, 14, 15, 16, 17, 16, 17, 18, 19, 20, 21, 20, 21, 22, 23, 24, 25, 24,
		25, 26, 27, 28, 29, 28, 29, 30, 31, 32, 1 };

int t[8][4][16] = { { { 14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7 },
		{ 0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8 }, { 4, 1, 14, 8,
				13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0 }, { 15, 12, 8, 2, 4, 9,
				1, 7, 5, 11, 3, 14, 10, 0, 6, 13 } }, { { 15, 1, 8, 14, 6, 11,
		3, 4, 9, 7, 2, 13, 12, 0, 5, 10 }, { 3, 13, 4, 7, 15, 2, 8, 14, 12, 0,
		1, 10, 6, 9, 11, 5 }, { 0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3,
		2, 15 }, { 13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9 } }, { {
		10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8 }, { 13, 7, 0, 9,
		3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1 }, { 13, 6, 4, 9, 8, 15, 3, 0,
		11, 1, 2, 12, 5, 10, 14, 7 }, { 1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3,
		11, 5, 2, 12 } }, { { 7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4,
		15 }, { 13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9 }, { 10, 6,
		9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4 }, { 3, 15, 0, 6, 10, 1,
		13, 8, 9, 4, 5, 11, 12, 7, 2, 14 } }, { { 2, 12, 4, 1, 7, 10, 11, 6, 8,
		5, 3, 15, 13, 0, 14, 9 }, { 14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3,
		9, 8, 6 }, { 4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14 }, {
		11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3 } }, { { 12, 1, 10,
		15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11 }, { 10, 15, 4, 2, 7, 12, 9,
		5, 6, 1, 13, 14, 0, 11, 3, 8 }, { 9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4,
		10, 1, 13, 11, 6 }, { 4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8,
		13 } }, { { 4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1 }, {
		13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6 }, { 1, 4, 11, 13,
		12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2 }, { 6, 11, 13, 8, 1, 4, 10, 7,
		9, 5, 0, 15, 14, 2, 3, 12 } }, { { 13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3,
		14, 5, 0, 12, 7 }, { 1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9,
		2 }, { 7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8 }, { 2, 1,
		14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11 } }

};

int P[] = { 16, 7, 20, 21, 29, 12, 28, 17, 1, 15, 23, 26, 5, 18, 31, 10, 2, 8,
		24, 14, 32, 27, 3, 9, 19, 13, 30, 6, 22, 11, 4, 25, };

int IP_INVERSE[64] = { 40, 8, 48, 16, 56, 24, 64, 32, 39, 7, 47, 15, 55, 23, 63,
		31, 38, 6, 46, 14, 54, 22, 62, 30, 37, 5, 45, 13, 53, 21, 61, 29, 36, 4,
		44, 12, 52, 20, 60, 28, 35, 3, 43, 11, 51, 19, 59, 27, 34, 2, 42, 10,
		50, 18, 58, 26, 33, 1, 41, 9, 49, 17, 57, 25, };

char* permutate(char *input, int *pc, int len) {
	char *output = (char*) malloc(sizeof(char) * len);
	for (int i = 0; i < len; i++) {
		output[i] = input[pc[i] - 1];
	}
	return output;
}

void left_shift(char *d, char *s, int shift) {
	int length = strlen(s);
	for (int i = 0; i < length - shift; i++) {
		d[i] = s[i + shift];
	}
	for (int i = 0; i < shift; i++) {
		d[i + length - shift] = s[i];
	}
}

char* get_kplus(char *key) {
	printf("Generated kplus with 56 bits by permutate with pc1 \n");
	return permutate(key, pc1, 56);
}

void generate_sub_keys(char **c, char **d, char **k, char **new_key,
		char *kplus) {
	printf("Generating Sub Keys \n");
	print_hex(kplus);
	copy(kplus, 0, 28, c[0]);
	copy(kplus, 28, 56, d[0]);
	printf("K+ is split split into c and d as 28 bits apart\n");

	concat(k[0], c[0], d[0]);

	printf("Generating (C1,D1),(C2,D2)....(C16,D16) by left shifting with shift array\n");

	for (int n = 1; n <= 16; n++) {
		left_shift(c[n], c[n - 1], shift_array[n - 1]);
		left_shift(d[n], d[n - 1], shift_array[n - 1]);
		concat(k[n], c[n], d[n]);
	}
	for (int n = 0; n < 17; n++) {
		printf("C%d=%s\n", n, c[n]);
		print_hex(c[n]);
		printf("D%d=%s\n", n, d[n]);
		print_hex(d[n]);

	}
	printf("(C1,D1),(C2,D2)....(C16,D16) are generated\n");
	printf("Going to generate new key by permuted with pc2. New Keys length is 48 bits\n");
	for (int n = 0; n < 16; n++) {
		strcpy(new_key[n], permutate(k[n + 1], pc2, 48));
		int l = strlen(new_key[n]);
		if (l > 48) {
			printf("Memory Leak\n");
			exit(1);
		}
		printf("New Key %d= ", n + 1);
		block_print(new_key[n], 6);
		print_hex(new_key[n]);
	}

	printf("New Key Generated \n");
}


int get_row(char *s) {
	char *t = (char*) malloc(sizeof(char) * 2);
	t[0] = s[0];
	t[1] = s[5];
	return get_decimal(t);
}

char* S(char *s, int m[][16]) {
	printf("SBOX %s\n",s);
	int row = get_row(s);
	int col = get_decimal(substr(s, 1, 5));
	int val = m[row][col];
	printf("Row (1 and 5th bit) %d col (middle 4 bits) %d val %d\n",row,col,val);
	char *binary_4_bit = get_binary(val, 4);
	printf("4 Bit Binary %s \n",binary_4_bit);
	return binary_4_bit;
}

void check(char *s, char *expected) {
	if (!debug)
		return;
	int c = strcmp(s, expected);
	if (c != 0) {
		printf("\nThings went wrong \n%s\n%s\n%d \n", s, expected, c);
		exit(1);
	}
}

char* expand(char *r) {
	char *er = (char*) malloc(sizeof(char) * 48);
	for (int i = 0; i < 48; i++) {
		er[i] = r[ebit[i] - 1];
	}
	return er;
}


char* f(char *r, char *k) {
	printf("Before Expanding...");
	print_hex(r);
	char *er = expand(r);
	printf("After Expanding...");
	print_hex(er);
	for (int i = 0; i < 48; i++) {
		er[i] = xor(er[i], k[i]);
	}
	printf("After XOR with k...");
	print_hex(er);
	printf("SBOX make 48 bit to 32 bit. For every 6 bit we will get a 4 bit\n");
	char *_32_bit = (char*) malloc(sizeof(char*) * 32);
	for (int i = 0; i < 8; i++) {
		char *_4_bit = S(substr(er, i * 6, (i + 1) * 6), t[i]);
		for (int j = 0; j < 4; j++) {
			_32_bit[i * 4 + j] = _4_bit[j];
		}
	}
	print_hex(_32_bit);
	char *a_p = permutate(_32_bit, P, 32);
	printf("After Permutate with P...");
	print_hex(a_p);
	return a_p;
}

void des_procedure() {
	printf("Data Encryption Standard.Symmetric Cryptography. Used to cipher and decipher the messages.\n");
	printf("http://page.math.tu-berlin.de/~kant/teaching/hess/krypto-ws2006/des.htm\n");
	printf("Get 64 bit key and 64 bit message. Make sure to pad in case they are less than 64 bit\n");
	printf("Find K Plus. K Plus is 56 bit by permute with PC1\n");
	printf("(C0,D0) is derived from KPlus as 28 bit each\n");
	printf("Find (C1,D1),(C2,D2)...(C16,D16) by left shifting with shift array\n");
	printf("Find new keys K1,K2...K16 by permute CnDn with PC2\n");
	printf("Permute Message with IP1\n");
	printf("Split (L0,R0) from the message \n");
	printf("Find (L1,R1),(L2,R2)... (L16,R16)\n");
	printf("L[N] = R[N-1], R[N] = XOR(L[N-1],FN(R[N-1],K[N-1])\n");
	printf("FN Explanation - Expand 32 bit to 48 bits by using Extended Bit table \n");
	printf("XOR each bit of expanded with key. For every block of 6 bits, reduce to 4 bits by S-Boxes\n");
	printf("S-Box. Find row by 1st and 6th Bit Binary Value\n Column by middle 4 bits.\n");
	printf("Take Intersection from T1,T2...T8. 32 bit scrambled message is permute with P Table\n");
	printf("Concatenate R16L16 and permute with IP_INVERSE. Get 64 bit ciphered message\n");
	printf("Deciphering - Same Process but keys should be applied in the order of K16,K15...K1\n");

}


void generateLR(char **l, char **r, char **new_key) {
	printf("Generating (L1,R1),(L2,R2)...(L16,R16) by S-Box and XOR \n");
	for (int n = 1; n <= 16; n++) {
		l[n] = r[n - 1];
		char *P = f(r[n - 1], new_key[n - 1]);
		printf("Find R(n) by xor l(n-1) and P(i)\n");
		for (int i = 0; i < 32; i++) {
			r[n][i] = xor(l[n - 1][i], P[i]);
		}
		printf("L%d ", n);
		block_print(l[n], 4);
		print_hex(l[n]);
		printf("R%d ", n);
		block_print(r[n], 4);
		print_hex(r[n]);
		print_line();
	}
	printf("Generated (L1,R1),(L2,R2)...(L16,R16)\n");
}

void generateLRReverse(char **l, char **r, char **new_key) {
	printf("Generating (L1,R1),(L2,R2)...(L16,R16) by S-Box and XOR \n");
	for (int n = 1; n <= 16; n++) {
		l[n] = r[n - 1];
		char *P = f(r[n - 1], new_key[17 - n - 1]);
		printf("Find R(n) by xor l(n-1) and P(i)\n");
		for (int i = 0; i < 32; i++) {
			r[n][i] = xor(l[n - 1][i], P[i]);
		}
		printf("L%d ", n);
		block_print(l[n], 4);
		print_hex(l[n]);
		printf("R%d ", n);
		block_print(r[n], 4);
		print_hex(r[n]);
		print_line();
	}
	printf("Generated (L1,R1),(L2,R2)...(L16,R16)\n");
}

char* encode(char **l, char **r) {
	printf("Concatenating R16 and L16 \n");
	char *K16 = (char*) malloc(sizeof(char*) * 64);
	for (int i = 0; i < 64; i++)
		K16[i] = '0';
	concat(K16, r[16], l[16]);
	printf("K16 ");
	block_print(K16, 8);
	printf("K16 %s \n", K16);
	print_hex(K16);
	printf("Encode after permuted with IP_INVERSE \n");
	char *E = permutate(K16, IP_INVERSE, 64);
	block_print(E, 8);
	print_hex(E);
	return E;
}

char* des_encryption(char *key, char *msg) {
	printf("Data Encryption Standard - Encryption\n");
	char *kplus = get_kplus(key);
	int kplus_length = strlen(kplus);
	printf("K+ generated of Length %d \n", kplus_length);

	char **c = (char**) malloc(sizeof(char**) * 17);
	char **d = (char**) malloc(sizeof(char**) * 17);
	char **k = (char**) malloc(sizeof(char**) * 17);
	char **new_key = (char**) malloc(sizeof(char**) * 16);
	for (int i = 0; i < 17; i++) {
		c[i] = (char*) malloc(sizeof(char*) * 28);
		d[i] = (char*) malloc(sizeof(char*) * 28);
		k[i] = (char*) malloc(sizeof(char*) * 56);
	}
	for (int i = 0; i < 16; i++) {
		new_key[i] = (char*) malloc(sizeof(char*) * 48);
	}
	generate_sub_keys(c, d, k, new_key, kplus);
	int block_size = 64;
	int no_of_blocks = strlen(msg) / block_size;
	char *FS = (char*) malloc(sizeof(char*) * no_of_blocks * block_size);
	printf("No of Blocks %d\n",no_of_blocks);
	for (int z = 0; z < no_of_blocks; z++) {
		char *message = substr(msg, z * block_size, (z + 1) * block_size);
		message = permutate(message, ip1, 64);
		printf("Message Permuted with IP1 table \n");
		char **l = (char**) malloc(sizeof(char**) * 17);
		char **r = (char**) malloc(sizeof(char**) * 17);
		for (int i = 0; i < 17; i++) {
			l[i] = (char*) malloc(sizeof(char*) * 32);
			r[i] = (char*) malloc(sizeof(char*) * 32);
		}
		copy(message, 0, 32, l[0]);
		copy(message, 32, 64, r[0]);
		printf("Message is split into 32 bits apart %s %s \n", l[0], r[0]);
		generateLR(l, r, new_key);
		char *C = encode(l, r);
		for (int x = 0; x < block_size; x++) {
			FS[z * block_size + x] = C[x];
		}
	}
	return FS;
}

char* des_decryption(char *key, char *msg) {
	printf("Data Encryption Standard - Decipher \n");
	int key_length = strlen(key);
	if(key_length != 64){
		printf("Key length is not 64\n");
		exit(1);
	}
	char *kplus = get_kplus(key);
	int kplus_length = strlen(kplus);
	if(kplus_length != 56){
		printf("Key plus length is not 56\n");
		exit(1);
	}
	printf("K+ generated of Length %d \n", kplus_length);

	char **c = (char**) malloc(sizeof(char**) * 17);
	char **d = (char**) malloc(sizeof(char**) * 17);
	char **k = (char**) malloc(sizeof(char**) * 17);
	char **new_key = (char**) malloc(sizeof(char**) * 16);
	for (int i = 0; i < 17; i++) {
		c[i] = (char*) malloc(sizeof(char*) * 28);
		d[i] = (char*) malloc(sizeof(char*) * 28);
		k[i] = (char*) malloc(sizeof(char*) * 56);
	}
	for (int i = 0; i < 16; i++) {
		new_key[i] = (char*) malloc(sizeof(char*) * 48);
	}

	generate_sub_keys(c, d, k, new_key, kplus);
	int block_size = 64;
	int no_of_blocks = strlen(msg) / block_size;
	char *FS = (char*) malloc(sizeof(char*) * no_of_blocks * block_size);
	printf("Message %s\n",msg);
	for (int z = 0; z < no_of_blocks; z++) {
		char *message = substr(msg, z * block_size, (z + 1) * block_size);
		message = permutate(message, ip1, 64);
		printf("Message Permuted with IP1 table \n");

		char **l = (char**) malloc(sizeof(char**) * 17);
		char **r = (char**) malloc(sizeof(char**) * 17);
		for (int i = 0; i < 17; i++) {
			l[i] = (char*) malloc(sizeof(char*) * 32);
			r[i] = (char*) malloc(sizeof(char*) * 32);
		}
		copy(message, 0, 32, l[0]);
		copy(message, 32, 64, r[0]);
		printf("Message is split into 32 bits apart %s %s \n", l[0], r[0]);
		generateLRReverse(l, r, new_key);
		char *P = encode(l, r);
		for (int x = 0; x < block_size; x++) {
			FS[z * block_size + x] = P[x];
		}
	}
	return FS;
}



char* get_key(char* prompt) {
	printf("%s (Max Length 8) ",prompt);
	char key[9];
	//scanf("%s", key);
	strcpy(key,"security");
	int len = strlen(key);
	if(len > 8) len = 8;
	int L = 8;
	char *M = (char*) malloc(sizeof(char*) * (L * 8));
	for (int i = 0; i < L; i++) {
		for (int j = 0; j < 8; j++) {
			int pos = i * 8 + j;
			M[pos] = '0';
		}
	}
	for (int i = 0; i < len; i++) {
		int d = key[i];
		char *X = get_binary(d, 8);
		int pos = (i * 8);
		for (int j = 0; j < 8; j++) {
			M[pos + j] = X[j];
		}
	}
	printf("Key %s \n",M);
	block_print(M, 8);
	return M;

}



char* get_hex_message() {
	int len = 1024;
	char* HEX = (char *)malloc(sizeof(char *)*len);
	printf("Enter the Hex Message (Max Length 1024)...");
	scanf("%s",HEX);
	len = strlen(HEX);
	char* binary = (char *)malloc(sizeof(char *)*4*len);
	for(int i = 0;i<len;i++){
		char* t = get_binary_from_hex(HEX[i]);
		for(int j = 0;j<4;j++){
			binary[(i*4) + j] = t[j];
		}
	}
	return binary;
}

char* get_message() {
	printf("Enter Message ");
	char message[100];
	scanf("%s", message);
	return get_64_bit_payload(message);
}

