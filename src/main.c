/*
 * Main.c
 *
 *  Created on: 20-Jul-2020
 *      Author: kkhan
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "caesar.h"
#include "playfair.h"
#include "vignere.h"
#include "rsa.h"
#include "hill.h"
#include "io.h"
#include "utils.h"
#include "diffie_hellman.h"
#include "dsa.h"
#include "des.h"
#include "binary_utils.h"
#include "triple_des.h"
#include "sha_1.h"
#include "aes.h"
#include "md5.h"



void caesar_test() {
	char *c = caesar_encryption("wewon", 3);
	printf("Ciphered Text %s\n", c);
	char *p = caesar_decryption(c, 3);
	printf("Deciphered Plain Text %s\n", p);
}

void caesar_encryption_test() {
	printf("Cipher Text %s\n",
			caesar_encryption(get_input("Enter Plain Text"),
					get_int_input("Enter K...")));
}

void caesar_decryption_test() {
	printf("Decipher Plain Text %s\n",
			caesar_decryption(get_input("Enter Cipher Text"),
					get_int_input("Enter K...")));
}

void playfair_encryption_test() {
	printf("Cipher Text %s\n",
			playfair_encryption(get_input("Enter Plain Text"),
					get_input("Enter key...")));

}

void playfair_decryption_test() {
	printf("Cipher Text %s\n",
			playfair_decryption(get_input("Enter Cipher Text"),
					get_input("Enter key...")));
}

void playfair_test() {
	char *cipher_text = playfair_encryption("wearediscoveredsaveyourself",
			"tutorial");
	printf("Cipher Text %s %ld\n", cipher_text, strlen(cipher_text));
	char *plain_text = playfair_decryption(cipher_text, "tutorial");
	printf("Plain Text %s\n", plain_text);
}

void vignere_encryption_test() {
	printf("Cipher Text %s\n",
			vignere_encryption(get_input("Enter Plain Text"),
					get_input("Enter key...")));
}

void vignere_decryption_test() {
	printf("Decipher Text %s\n",
			vignere_decryption(get_input("Enter Cipher Text"),
					get_input("Enter key...")));
}

void vignere_test() {
	char *cipher_text = vignere_encryption("attackfromsoutheast", "point");
	printf("Cipher Text %s\n", cipher_text);
	char *plain_text = vignere_decryption(cipher_text, "point");
	printf("Plain Text Back %s\n", plain_text);
}

void hill_encryption_test() {
	printf("Cipher Text %s\n",
			hill_encryption(get_input("Enter Plain Text"),
					get_input("Enter key..."),
					get_int_input("Enter Matrix Size ")));
}

void hill_decryption_test() {
	printf("Decipher Text %s\n",
			hill_decryption(get_input("Enter Cipher Text"),
					get_input("Enter key..."),
					get_int_input("Enter Matrix Size ")));
}

void hill_test() {
	char *cipher_text = hill_encryption("actsmarte", "gybnqkurp", 3);
	printf("Cipher Text %s\n", cipher_text);
	char *plain_text = hill_decryption(cipher_text, "gybnqkurp", 3);
	printf("Plain Text Back : %s\n", plain_text);
}

void des_encryption_test() {
	char *key = get_key("Key");
	char *message = get_message();
	char *cipher_text = des_encryption(key, message);
	printf("Cipher Text :%s\n", cipher_text);
	print_hex(cipher_text);
}

void des_decryption_test() {
	char *key = get_key("Key");
	char *message = get_hex_message();
	char *plain_text = des_decryption(key,message);
	print_hex(plain_text);
	printf("Plain Text : %s\n", get_character_from_binary(plain_text));
	printf("%s\n",plain_text);

}

void des_test() {
	char *key = "0110100101101110011001000110100101100001000000000000000000000000";
	char *message = get_64_bit_payload("india is my country");
	char *cipher_text = des_encryption(key, message);
	printf("Cipher Text %s\n", cipher_text);
	char *plain_text = des_decryption(key, cipher_text);
	printf("Plain Text Back : %s\n", get_character_from_binary(plain_text));
}

void triple_des_encryption_test() {
	char *key1 = get_key("Key1");
	char *key2 = get_key("Key2");
	char *message = get_message();
	char *cipher_text = triple_des_encryption(key1,key2, message);
	printf("Cipher Text : %s\n", cipher_text);
	print_hex(cipher_text);
}

void triple_des_decryption_test() {
	char *key1 = get_key("Key1");
	char *key2 = get_key("Key2");
	char *message = get_hex_message();
	char *plain_text = triple_des_decryption(key1,key2,message);
	print_hex(plain_text);
	printf("Plain Text : %s\n", get_character_from_binary(plain_text));
	printf("%s\n",plain_text);

}

void triple_des_test() {
	char *key = "0110100101101110011001000110100101100001000000000000000000000000";
	char *message = get_64_bit_payload("india is my country");
	char *cipher_text = des_encryption(key, message);
	printf("Cipher Text %s\n", cipher_text);
	char *plain_text = des_decryption(key, cipher_text);
	printf("Plain Text Back : %s\n", get_character_from_binary(plain_text));
}

void rsa_int_test(int e, int d, int n) {
	int m = get_int_input("Enter M");
	int c = rsa_encryption(m, e, n);
	printf("Ciphered Integer...%d\n", c);
	int p = rsa_decryption(c, d, n);
	printf("Deciphered Integer...%d\n", p);

}

void rsa_message_test(int e, int d, int n) {
	char *message = get_input("Enter Message");
	int *cipher_array = (int*) malloc(sizeof(int) * strlen(message));
	int *plain_array = (int*) malloc(sizeof(int) * strlen(message));

	printf("ciphering Started\n");
	for (int i = 0; i < strlen(message); i++) {
		int c = rsa_encryption(message[i] - 97, e, n);
		cipher_array[i] = c;
	}
	printf("Ciphered Data...");
	for (int i = 0; i < strlen(message); i++) {
		printf("%d ", cipher_array[i]);
	}
	printf("\nDeciphering Started\n");

	for (int i = 0; i < strlen(message); i++) {
		int plain = rsa_decryption(cipher_array[i], d, n);
		plain_array[i] = plain;
	}
	printf("Deciphered Data\n");
	for (int i = 0; i < strlen(message); i++) {
		printf("%c", plain_array[i] + 97);
	}
	print_line();
}

void rsa_test() {
	rsa_procedure();
	int p = get_prime("Enter P");
	int q = get_prime("Enter Q");
	int n = p * q;
	int tc = 0;
	int e, d;
	generate_key(p, q, &e, &d, &tc);
	int option = get_int_input("Options 1-Integer 2-String ");
	switch (option) {
	case 1:
		rsa_int_test(e, d, n);
		break;
	case 2:
		rsa_message_test(e, d, n);
		break;
	}
}

void aes_encryption_test() {
	char *key = get_input("Key");
	char *message = get_input("Message");
	uint32_t* cipher_text = aes_encryption(message,key);
	for(int i = 0;i<16;i++){
		printf("%02x", cipher_text[i]);
	}
	printf("\n");
}

void aes_decryption_test() {
	char *key = get_input("Key");
	char *message = (char *)malloc(sizeof(char *)*32);
	printf("Enter Message...");
	scanf("%s",message);
	int len = strlen(message);
	printf("message %s len %d\n",message,len);
	//char *key = "madurai";
	//char *message = "78367670eec8946569a8d19f998a2484";
	uint32_t* o = (uint32_t*)malloc(sizeof(uint32_t *) * 16);
	for(int i = 0;i<16;i++){
		o[i] = 0;
	}
	int k = 0;
	for(int i = 0;i<16;i++){
		char c = message[i*2];
		char d = message[i*2+1];
		int d1 = get_decimal_from_hex(c);
		int d2 = get_decimal_from_hex(d);
		o[i] = d1*16 + d2;
		printf("%c%c %d %d %x\n",c,d,d1,d2,o[i]);
	}

	printf("\n");
	char *plain_text = aes_decryption(o,key);
	printf("clear text : %s\n",plain_text);
}

void aes_test() {
	char *key = get_input("Key");
	char *message = get_input("Message");
	uint32_t* cipher_text = aes_encryption(message,key);
	for(int i = 0;i<16;i++){
		printf("%x", cipher_text[i]);
	}
	char* clear_text = aes_decryption(cipher_text,key);
	printf("\nClear Text Back : %s\n",clear_text);
}

void runner(void (*procedure)(), void (*encryption_test)(),
		void (*decryption_test)(), void (*test)()) {
	int C = 1;
	print_line();
	while (C) {
		int option =
				get_int_input(
						"Options \n1-Procedure \n2-Cipher \n3-Decipher \n4-Both Cipher and Decipher \n5-Break\nEnter Your Choice...");
		switch (option) {
		case 1:
			procedure(); break;
		case 2:
			encryption_test();
			break;
		case 3:
			decryption_test();
			break;
		case 4:
			test();
			break;
		case 5:
			C = 0;
			break;
		}
	}
	print_line();
}

void sha1_digest(){
	printf("%s\n",digest(get_input("Enter the Message...")));
}

void md5digest(){
	printf("%s\n",md5_digest(get_input("Enter the Message...")));
}


void digest_runner(void (*procedure)(),void (*digest)()) {
	int C = 1;
	print_line();
	while (C) {
		int option =
				get_int_input(
						"Options \n1-Procedure \n2-Digest \n3-Break\nEnter Your Choice...");
		switch (option) {
		case 1:
			procedure(); break;
		case 2:
			digest();break;
			break;
		case 3:
			C = 0;
			break;
		}
	}
	print_line();
}

void test_diffie_hellman() {
	diffie_hellman_procedure();
	int P = get_prime("Enter P (Prime)");
	int Q = get_int_input("Enter Q (Prime Root)");

	int a = get_int_input("One End's Private Key");
	int b = get_int_input("Another End's Private Key");

	int x = get_public_exchange_number(P, Q, a);
	int y = get_public_exchange_number(P, Q, b);

	printf("x = %d y = %d \n", x, y);

	int secret_key1 = get_secret_key(y, a, P);
	int secret_key2 = get_secret_key(x, b, P);

	printf("secret_key at one end = %d secret_key at another end = %d \n",
			secret_key1, secret_key2);
}

void test_dsa() {
	int p = get_int_input("Please Enter p");
	int q = get_int_input("Please Enter q");
	int g = get_int_input("Please Enter g");
	int private_key, public_key, r, s;

	generate_dsa_key(p, q, g, &private_key, &public_key);
	char *M = get_input("Enter Message");
	generate_signature(p, q, g, private_key, M, &r, &s);
	verify_signature(p, q, g, public_key, M, r, s);

	printf("\nTamper r to check whether it is authenticated message\n");
	r = get_int_input("Enter tampered R");
	verify_signature(p, q, g, public_key, M, r, s);

}

int main() {
	while (1) {
		int option =
				get_int_input(
						"Select Cipher \n1-Ceasar \n2-PlayFair \n3-Vignere \n4-Hill \n5-RSA \n6-Diffie-Hellman \n7-DSA \n8-DES \n9-Triple DES \n10-SHA-1 \n11-AES-128 \n12-MD5 \n13-Quit...\nEnter your choice...");
		switch (option) {
		case 1:
			runner(caesar_procedure, caesar_encryption_test,
					caesar_decryption_test, caesar_test);
			break;
		case 2:
			runner(playfair_procedure, playfair_encryption_test,
					playfair_decryption_test, playfair_test);
			break;
		case 3:
			runner(vignere_procedure, vignere_encryption_test,
					vignere_decryption_test, vignere_test);
			break;
		case 4:
			runner(hill_procedure, hill_encryption_test, hill_decryption_test,
					hill_test);
			break;
		case 5:
			rsa_test();
			break;
		case 6:
			test_diffie_hellman();
			break;
		case 7:
			test_dsa();
			break;
		case 8:
			runner(des_procedure, des_encryption_test, des_decryption_test,
					des_test);
			break;
		case 9:
			runner(triple_des_procedure, triple_des_encryption_test, triple_des_decryption_test,
							des_test);
			break;
		case 10:
			digest_runner(sha_1_procedure,sha1_digest);
			break;
		case 11:
			runner(aes_procedure, aes_encryption_test, aes_decryption_test,
							aes_test);
			break;
		case 12:
			digest_runner(md5_procedure,md5digest);
		case 13:
			exit(1);
		}
	}

}

