#include "math_utils.h"
#include "hill.h"
#include "utils.h"
#include "matrix.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


int** make_key_matrix(char* key,int n){
	int** matrix = (int **)malloc(sizeof(int *) * n);
	for(int i = 0;i<n;i++){
		matrix[i] = (int *)malloc(sizeof(int) * n);
	}
	int length = strlen(key);
	int extra = 0;
	int k = 0;
	for(int i = 0;i<n;i++){
		for(int j = 0;j<n;j++){
			if(k < length){
				matrix[i][j] = get_integer(key[k++]);
			}else{
				matrix[i][j] = extra++;
			}
		}
	}
	return matrix;
}

char* hill_encryption_x(char* plain_text,int **key,int len){
	int MODULUS = get_modulus();
	int* values = (int *)malloc(sizeof(int) * strlen(plain_text));
	char* cipher_text = (char *)malloc(sizeof(char) * strlen(plain_text));
	int length = strlen(plain_text);
	for(int i = 0;i<length;i++){
		int d = get_integer(plain_text[i]);
		values[i] = d;
	}

	for(int i = 0;i<len;i++){
		int sum = 0;
		for(int j = 0;j<len;j++){
			sum = sum + (values[j] * key[i][j]);
		}
		int mod = sum % MODULUS;
		cipher_text[i] = get_char(mod);
	}

	return cipher_text;
}


char* hill_encryption_matrix(char* plain_text,int** matrix,int n){
	int length = strlen(plain_text);
	char* cipher_text = (char *)malloc(sizeof(char) * strlen(plain_text));
	int index = 0;
	while(index < length){
		char* c = hill_encryption_x(substring(plain_text,index,n),matrix,n);
		strcat(cipher_text,c);
		index = index + n;
	}
	return cipher_text;
}

char* hill_encryption(char* plain_text,char *key,int n){
	int MODULUS = get_modulus();
	int **matrix = make_key_matrix(key,n);
	printf("Key Matrix...\n");
	print_matrix_2d(matrix,n);
	int D = determinant(matrix,n);
	if(D == 0){
		printf("Not Possible to find Inverse as determinant value is zero\n");
		return "";
	}
	if(D < 0){
		D = reduce(D,MODULUS);
	}
	printf("Determinant Value %d\n",D);
	int _gcd = gcd(D,MODULUS);
	printf("GCD (%d,%d) %d\n",D,MODULUS,_gcd);
	if(_gcd != 1){
		printf("Not co prime. not possible\n");
		return "";
	}
	return hill_encryption_matrix(plain_text,matrix,n);
}



char* hill_decryption(char* cipher_text,char *key,int n){
	int MODULUS = get_modulus();
	int** matrix = make_key_matrix(key,n);
	print_matrix_2d(matrix,n);
	int** cofactor_matrix = (int **)malloc(sizeof(int *) * n);
	int** adjoint_matrix = (int **)malloc(sizeof(int *) * n);
	int** inverse_matrix = (int **)malloc(sizeof(int *) * n);

	for(int i = 0;i<n;i++){
		cofactor_matrix[i] = (int *)malloc(sizeof(int) * n);
		adjoint_matrix[i] = (int *)malloc(sizeof(int) * n);
		inverse_matrix[i] = (int *)malloc(sizeof(int) * n);
	}

	int X = 1;
	for(int i = 0;i<n;i++){
		for(int j = 0;j<n;j++){
			X = sign_of_position(i,j);
			int** rm = reduce_matrix(matrix,i,j,n);
			int d = X * determinant(rm,n-1);
			cofactor_matrix[i][j] = d;

		}
	}
	//print_matrix_2d(cofactor_matrix,n);

	int D = determinant(matrix,n);
	if(D < 0){
		D = reduce(D,MODULUS);
	}
	int inv = inverse(D,MODULUS);
	for(int i = 0;i<n;i++){
		for(int j = 0;j<n;j++){
			adjoint_matrix[i][j] = cofactor_matrix[j][i];
			if(adjoint_matrix[i][j] < 0){
				adjoint_matrix[i][j] = reduce(adjoint_matrix[i][j],MODULUS);
			}
			int x = adjoint_matrix[i][j] * inv;
			inverse_matrix[i][j] = x % MODULUS;
		}
	}
	printf("Inverse Matrix \n");
	print_matrix_2d(inverse_matrix,n);
	char* plain_text = hill_encryption_matrix(cipher_text,inverse_matrix,n);
	return plain_text;
}

void hill_procedure(){

}

