#include "vignere.h"
#include "utils.h"
#include "matrix.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

char* vignere_substitution(char* input_text,char* key,int mode){
	int MODULUS = get_modulus();
	printf("Input Text = %s Key = %s\n", input_text,key);
	char* output_text = (char *)malloc(sizeof(char) * strlen(input_text));
	for(int i=0,j=0;i<strlen(input_text);i++){
		if(j == strlen(key)){
			j = 0;
		}
		int k = get_integer(key[j]);
		int x = get_integer(input_text[i]);
		output_text[i] = get_char(shift(x,k,MODULUS,mode));
		j++;
	}
	return output_text;
}

char* vignere_encryption(char* plain_text,char* key){
	return vignere_substitution(plain_text,key,ENCRYPTION);
}


char* vignere_decryption(char* cipher_text,char* key){
	return vignere_substitution(cipher_text,key,DECRYPTION);
}


void vignere_procedure(){
	printf("Poly alphabetic substitution, Symmetric Cipher\n");
	printf("Repeat the key to match the plain or cipher text\n");
	printf("Add corresponding key with plain text and take modulus 26\n");
	printf("For Decipher subtract the key with plain text and take modulus 26 \n");
}
