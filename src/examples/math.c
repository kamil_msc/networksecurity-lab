#include "math_utils.h"
#include "io.h"
#include <stdio.h>

void x(int a,int b,int n,int e){
	printf("Power Mod Checking %d ^ %d mod %d = %d = %d \n",a,b,n,power_mod(a,b,n),e);
}

int main(){
	//x(25,22,30,25);
	x(82,get_int_input("Enter Exponent"),1022117,582624);
	x(12,34,56,16);
}

