#include "dsa.h"
#include "io.h"
#include <stdio.h>

int main(){

	int p = 283;
	int q = 47;
	int g = 60;
	int a = 24;
	int b = 158;


	int r,s;
	generate_dsa_key(p,q,g,&a,&b);
	generate_signature(p,q,g,a,"CUTN",&r,&s);
	verify_signature(p,q,g,b,"CUTN",r,s);

}

