#include "rsa.h"
#include "io.h"
#include <stdio.h>

int main(){

	int p = 23;
	int q = 11;
	int n = p*q;
	int e,d,tc;

	generate_key(p,q,&e,&d,&tc);
	int m = 28;
	int c = rsa_encryption(m,e,n);
	printf("Ciphered Integer...%d\n",c);
	int plain = rsa_decryption(c,d,n);
	printf("Deciphered Integer...%d\n",plain);
	if(m == plain){
		printf("Algorithm seems to be working\n");
	}else{
		printf("Algorithm Failed\n");
	}

}

