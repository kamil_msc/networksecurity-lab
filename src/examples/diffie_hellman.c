#include "diffie_hellman.h"
#include "io.h"
#include <stdio.h>

int main(){

	int P = 541;
	int Q = 10;

	int a = 5;
	int b = 7;

	//P = 23; Q = 9;a = 4; b = 3;
	int x = get_public_exchange_number(P,Q,a);
	int y = get_public_exchange_number(P,Q,b);
	printf("x = %d y = %d \n",x,y);

	int secret_key1 = get_secret_key(y,a,P);
	int secret_key2 = get_secret_key(x,b,P);

	printf("secret_key1 = %d secret_key2 = %d \n",secret_key1,secret_key2);
	/*
	int found = 0;
	for(int i = 2;i<10000;i++){
		if(i != b){
			int s = get_secret_key(x,i,P);
			//printf("secret key guessed by the attacker for %d = %d\n",i,s);
			if(s == secret_key2){
				found = 1;
				printf("Get the same key at i = %d",i);
				break;
			}
		}
	}
	if(!found){
		printf("\nCan't get the same secret key when guessed between 2 to 100\n");
	}
	*/

}

