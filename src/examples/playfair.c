#include "playfair.h"
#include <stdio.h>
#include <string.h>

int main(){
	char* cipher_text = playfair_encryption("wearediscoveredsaveyourself","tutorial");
	//char* cipher_text = playfair_encryption("saveyourself","tutorial");
	printf("\nCipher Text %s %ld\n",cipher_text,strlen(cipher_text));
	char* plain_text = playfair_decryption(cipher_text,"tutorial");
	printf("\nPlain Text %s\n",plain_text);
}

