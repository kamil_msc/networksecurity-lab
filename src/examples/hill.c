#include "hill.h"
#include <stdio.h>

int main(){
	int n = 2;
	char* cipher_text = hill_encryption("shortexample","hill",n);
	printf("%s\n",cipher_text);
	char* plain_text = hill_decryption(cipher_text,"hill",n);
	printf("%s\n",plain_text);

	n = 3;
	cipher_text = hill_encryption("retreatnow","backupabc",n);
	printf("%s\n",cipher_text);
	plain_text = hill_decryption(cipher_text,"backupabc",n);
	printf("%s\n",plain_text);
}

