#include "aes.h"
#include "io.h"

#include "binary_utils.h"
#include "String_utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

void test() {
	//uint32_t* cipher = aes_encryption("hello attack from north east. If they are defensive, attack from west. If they are offensive, get back to us","attack");
	uint32_t* cipher = aes_encryption("hello attack from north east.","attack");

	print_line();
	char* clear = aes_decryption(cipher,"attack");
	printf("clear text %s\n",clear);
}



int main() {
	test();

}

