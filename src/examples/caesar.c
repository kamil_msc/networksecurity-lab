#include "caesar.h"
#include <stdio.h>


int main(){
	char* c = caesar_encryption("attacklondonfromsoutheast30nauticalmiles",3);
	printf("Ciphered Text %s\n",c);
	char* p = caesar_decryption(c,3);
	printf("Deciphered Plain Text %s\n",p);
}

