#include "triple_des.h"
#include "io.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "binary_utils.h"
#include "String_utils.h"
#include "des.h"



void cipher() {
	char *key1 = get_key("Key1");
	block_print(key1, 8);
	print_hex(key1);
	char *key2 = get_key("Key2");
	block_print(key2, 8);
	print_hex(key2);

	char *message = get_64_bit_payload("Hello");

	char *C = triple_des_encryption(key1,key2, message);
	printf("Ciphered Text %s\n", C);
	print_hex(C);

	key1 = get_key("Key1");
	key2 = get_key("Key2");
	char *P = triple_des_decryption(key1,key2, C);
	printf("Plain Text %s\n",P);
	printf("%s\n",get_character_from_binary(P));

}



int main() {
	cipher();

}

