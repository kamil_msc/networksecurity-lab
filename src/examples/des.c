#include "des.h"
#include "io.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "binary_utils.h"
#include "String_utils.h"



void cipher() {
	char *key = get_key("Enter the Key to Encrypt ");
	block_print(key, 8);
	print_hex(key);

	char *message =
			get_64_bit_payload("coimbatore is south india's manchester.");
	/*
	char input[20];
	printf("Enter Message...");
	scanf("%s",input);

	char *message = get_64_bit_payload(input);
	block_print(message, 8);
	*/

	char *C = des_encryption(key, message);
	printf("Ciphered Text %s\n", C);
	print_hex(C);


	key = get_key("Enter the Key to Decrypt ");
	char *P = des_decryption(key,C);
	printf("Plain Text %s\n",P);
	printf("%s\n",get_character_from_binary(P));

}



int main() {
	cipher();

}

