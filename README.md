# NetworkSecurity-Lab


1.  Ceasar Cipher/Decipher
2.  PlayFair Cipher/Decipher
3.  Vignere Cipher/Decipher
4.  Hill Cipher/Decipher
5.  RSA Algorithm
6.  Diffie-Hellman Key Sharing
7.  DSA - Digital Signature Algorithm
8.  Data Encryption Standard
9.  Triple DES
10. SHA-1
11. AES-128
