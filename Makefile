lab: lib obj/md5.o obj/sha_1.o obj/triple_des.o obj/aes.o obj/main.o obj/dsa.o obj/vignere.o obj/playfair.o  obj/rsa.o obj/hill.o obj/caesar.o obj/diffie_hellman.o obj/des.o 
	gcc -o lab obj/*.o
	
lib: obj/utils.o obj/matrix.o  obj/io.o obj/math_utils.o obj/io.o obj/string_utils.o obj/binary_utils.o

obj/sha_1.o	: src/sha_1.c include/sha_1.h
	gcc -Iinclude -c src/sha_1.c -o obj/sha_1.o	
	
obj/md5.o	: src/md5.c include/md5.h
	gcc -Iinclude -c src/md5.c -o obj/md5.o	

obj/dsa.o	: src/dsa.c 
	gcc -Iinclude -c src/dsa.c -o obj/dsa.o	
	
obj/des.o	: src/des.c 
	gcc -Iinclude -c src/des.c -o obj/des.o	

obj/aes.o	: src/aes.c 
	gcc -Iinclude -c src/aes.c -o obj/aes.o	
	
obj/triple_des.o	: src/triple_des.c 
	gcc -Iinclude -c src/triple_des.c -o obj/triple_des.o		
	
obj/diffie_hellman.o	: src/diffie_hellman.c 
	gcc -Iinclude -c src/diffie_hellman.c -o obj/diffie_hellman.o
	
obj/hill.o	: src/hill.c include/hill.h
	gcc -Iinclude -c src/hill.c -o obj/hill.o	
	
obj/caesar.o	: src/caesar.c include/caesar.h
	gcc -Iinclude -c src/caesar.c -o obj/caesar.o

obj/playfair.o	: src/playfair.c include/playfair.h
	gcc -Iinclude -c src/playfair.c -o obj/playfair.o

obj/rsa.o		: src/rsa.c 
	gcc -Iinclude -c src/rsa.c -o obj/rsa.o

obj/vignere.o	: src/vignere.c 
	gcc -Iinclude -c src/vignere.c -o obj/vignere.o

obj/main.o	: src/main.c 
	gcc -Iinclude -c src/main.c -o obj/main.o

obj/utils.o	: src/utils/utils.c 
	gcc -Iinclude -c src/utils/utils.c -o obj/utils.o

obj/string_utils.o	: src/utils/string_utils.c 
	gcc -Iinclude -c src/utils/string_utils.c -o obj/string_utils.o

obj/matrix.o	: src/utils/matrix.c 
	gcc -Iinclude -c src/utils/matrix.c -o obj/matrix.o

obj/math_utils.o	: src/utils/math_utils.c
	gcc -Iinclude -c src/utils/math_utils.c -o obj/math_utils.o

obj/binary_utils.o	: src/utils/binary_utils.c
	gcc -Iinclude -c src/utils/binary_utils.c -o obj/binary_utils.o
	
obj/io.o	: src/utils/io.c 
	gcc -Iinclude -c src/utils/io.c -o obj/io.o 
	
hill: src/examples/hill.c obj/hill.o lib
	gcc -Iinclude -c src/examples/hill.c -o obj/examples/hill.o
	gcc -o hill obj/examples/hill.o obj/hill.o obj/matrix.o obj/math_utils.o obj/utils.o

playfair: src/examples/playfair.c obj/playfair.o lib
	gcc -Iinclude -c src/examples/playfair.c -o obj/examples/playfair.o
	gcc -o playfair obj/examples/playfair.o obj/playfair.o obj/matrix.o obj/math_utils.o obj/utils.o

caesar: src/examples/caesar.c obj/caesar.o lib
	gcc -Iinclude -c src/examples/caesar.c -o obj/examples/caesar.o
	gcc -o caesar obj/examples/caesar.o obj/caesar.o obj/matrix.o obj/math_utils.o obj/utils.o

vignere: src/examples/vignere.c obj/vignere.o lib
	gcc -Iinclude -c src/examples/vignere.c -o obj/examples/vignere.o
	gcc -o vignere obj/examples/vignere.o obj/vignere.o obj/matrix.o obj/math_utils.o obj/utils.o

diffie_hellman: src/examples/diffie_hellman.c obj/diffie_hellman.o lib
	gcc -Iinclude -c src/examples/diffie_hellman.c -o obj/examples/diffie_hellman.o
	gcc -o diffie_hellman obj/examples/diffie_hellman.o obj/diffie_hellman.o  obj/math_utils.o obj/utils.o obj/io.o

dsa: src/examples/dsa.c obj/dsa.o lib
	gcc -Iinclude -c src/examples/dsa.c -o obj/examples/dsa.o
	gcc -o dsa obj/examples/dsa.o obj/dsa.o  obj/math_utils.o obj/utils.o obj/io.o


des: src/examples/des.c obj/des.o lib
	gcc -Iinclude -c src/examples/des.c -o obj/examples/des.o
	gcc -o des obj/examples/des.o obj/des.o  obj/math_utils.o obj/utils.o obj/io.o obj/string_utils.o obj/binary_utils.o

triple_des: src/examples/triple_des.c obj/triple_des.o lib
	gcc -Iinclude -c src/examples/triple_des.c -o obj/examples/triple_des.o
	gcc -o triple_des obj/examples/triple_des.o obj/triple_des.o obj/des.o  obj/math_utils.o obj/utils.o obj/io.o obj/string_utils.o obj/binary_utils.o

sha_1: src/examples/sha_1.c obj/sha_1.o lib
	gcc -Iinclude -c src/examples/sha_1.c -o obj/examples/sha_1.o
	gcc -o sha_1 obj/examples/sha_1.o obj/sha_1.o  obj/math_utils.o obj/utils.o obj/io.o obj/string_utils.o obj/binary_utils.o


rsa: src/examples/rsa.c obj/rsa.o lib
	gcc -Iinclude -c src/examples/rsa.c -o obj/examples/rsa.o
	gcc -o rsa obj/examples/rsa.o obj/rsa.o  obj/math_utils.o obj/utils.o obj/io.o

aes: src/examples/aes.c obj/aes.o lib
	gcc -Iinclude -c src/examples/aes.c -o obj/examples/aes.o
	gcc -o aes obj/examples/aes.o obj/aes.o  obj/math_utils.o obj/utils.o obj/io.o obj/string_utils.o obj/binary_utils.o


math: src/examples/math.c  lib
	gcc -Iinclude -c src/examples/math.c -o obj/examples/math.o
	gcc -o math obj/examples/math.o   obj/math_utils.o obj/utils.o obj/io.o

md5: src/examples/md5.c obj/md5.o lib
	gcc -Iinclude -c src/examples/md5.c -o obj/examples/md5.o
	gcc -o md5 obj/examples/md5.o obj/md5.o  obj/math_utils.o obj/utils.o obj/io.o obj/string_utils.o obj/binary_utils.o



build: lib hill vignere caesar playfair diffie_hellman dsa rsa lab des sha_1 triple_des aes md5

clean:
	rm -f obj/*.o obj/examples/*.o
	rm -f lab lib hill vignere playfair caesar dsa rsa des diffie_hellman math sha_1 triple_des aes
	
	
	