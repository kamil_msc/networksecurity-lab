/*
 * binary_utils.h
 *
 *  Created on: 20-Jul-2020
 *      Author: kkhan
 */

#include<stdint.h>
#ifndef binary_utils
#define binary_utils

uint32_t get_decimal(char *dst);
char* get_binary(uint32_t decimal,int length);
char hex(char *s);
char* get_binary_from_hex(char h);
void print_hex(char *s);
char* get_hex(char *B);
char* get_character_from_binary(char *B);
char* get_64_bit_payload(char *message);

char xor(char a, char b);
char or(char a, char b);
char and(char a, char b);

char* xor_word(char *a, char *b);
char* and_word(char *a,char *b);
char* or_word(char *a,char *b);
char* not_word(char *a);

char* left_rotate(char *a,int len);

char* get_binary_from_hex_string(char* hex_string);
int get_hex_from_binary(char *bin);
int get_decimal_from_hex(char hex);

#endif
