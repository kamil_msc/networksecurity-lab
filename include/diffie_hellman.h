/*
 * diffie_hellman.h
 *
 *  Created on: 24-Aug-2020
 *      Author: kkhan
 */

#ifndef diffie_hellman
#define diffie_hellman


int get_public_exchange_number(int P,int Q,int private_key);
int get_secret_key(int exchanged_no,int private_key,int P);
void diffie_hellman_procedure();

#endif
