/*
 * playfair.h
 *
 *  Created on: 20-Jul-2020
 *      Author: kkhan
 */

#ifndef playfair
#define playfair


char* playfair_encryption(char* plain_text,char* key);
char* playfair_decryption(char* cipher_text,char* key);
void playfair_procedure();

#endif
