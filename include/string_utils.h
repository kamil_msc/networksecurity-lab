/*
 * string_utils.h
 *
 *  Created on: 20-Jul-2020
 *      Author: kkhan
 */

#ifndef string_utils
#define string_utils

void concat(char *dst, char *src_1, char* src_2);
void copy(char *src, int s, int e, char*dst);
void block_print(char *s,int block_size);
void block_print_ext(char *s,int block_size,char* ext);
char* substr(char *er, int s, int e);

#endif
