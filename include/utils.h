/*
 * utils.h
 *
 *  Created on: 20-Jul-2020
 *      Author: kkhan
 */

#ifndef utils
#define utils

# define ENCRYPTION 1
# define DECRYPTION 2

char* append_char_if_odd(char* plain_text,char c);
int is_char_not_present(char *stg,char c);
char* get_unique_letters(char* key);
char* replace(char* word, char s,char d);
int reduce(int c,int v);
char* substring(char *string,int index,int length);
void print_line();
int get_integer(char c);
char get_char(int no);
int get_modulus();
int shift(int n,int k,int MODULUS,int mode);
#endif
