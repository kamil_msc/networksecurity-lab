#ifndef dsa
#define dsa


void generate_dsa_key(int p,int q,int g,int *x,int *y);
void generate_signature(int p,int q,int g,int signer_private_key,char *M,int *r,int *s);
int verify_signature(int p,int q,int g,int signer_public_key,char *SM,int r,int s);

#endif
