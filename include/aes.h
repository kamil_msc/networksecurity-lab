#include <stdint.h>

#ifndef aes
#define aes


uint32_t* aes_encryption(char* key,char*message);
char* aes_decryption(uint32_t* key,char*message);
void aes_procedure();

#endif
