/*
 * rsa.h
 *
 *  Created on: 3-Aug-2020
 *      Author: kkhan
 */

#ifndef rsa
#define rsa


int rsa_encryption(int message,int e,int n);
int rsa_decryption(int message,int d,int n);
void generate_key(int p,int q,int *e,int *d, int *tc);
void rsa_procedure();

#endif
