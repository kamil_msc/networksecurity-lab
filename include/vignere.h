/*
 * vignere.h
 *
 *  Created on: 20-Jul-2020
 *      Author: kkhan
 */

#ifndef vignere
#define vignere


char* vignere_encryption(char* plain_text,char* key);
char* vignere_decryption(char* cipher_text,char* key);
void vignere_procedure();

#endif
