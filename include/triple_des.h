#ifndef triple_des
#define triple_des



char* triple_des_encryption(char* key1,char* key2,char*message);
char* triple_des_decryption(char* key,char* key2,char*message);
void triple_des_procedure();

#endif
