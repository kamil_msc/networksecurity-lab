/*
 * vignere.h
 *
 *  Created on: 20-Jul-2020
 *      Author: kkhan
 */

#ifndef io
#define io


char* get_input(char *message);
int get_int_input(char *message);
int get_prime(char* message);
void print_line();
int get_random(int lower,int upper);
void print_binary_string();

#endif
