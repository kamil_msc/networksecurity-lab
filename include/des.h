#ifndef des
#define des


char* des_encryption(char* key,char*message);
char* des_decryption(char* key,char*message);
char* get_key(char* prompt);
char* get_message();
void des_procedure();
char* get_hex_message();

#endif
