/*
 * math_utils.h
 *
 *  Created on: 3-Aug-2020
 *      Author: kkhan
 */

#include<stdint.h>

#ifndef math_utils
#define math_utils


int is_prime(int no);
int gcd(int a,int b);
int multiplicative_inverse(int no,int mod);
int greatest_power_of_2(int no);
uint32_t power(int m,int n);
int highest_bit_location(int no);
int mod(int a,int b);
int inverse(int D,int modulus);
int power_mod(int a,int b,int P);
#endif
