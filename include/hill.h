/*
 * hill.h
 *
 *  Created on: 20-Jul-2020
 *      Author: kkhan
 */

#ifndef hill
#define hill


char* hill_encryption(char* plain_text,char *key,int n);
char* hill_decryption(char* cipher_text,char *key,int n);
void hill_procedure();

#endif
